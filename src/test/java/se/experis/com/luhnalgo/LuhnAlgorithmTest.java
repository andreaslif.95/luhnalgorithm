package se.experis.com.luhnalgo;

import org.junit.jupiter.api.Test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.Duration;

import static org.junit.jupiter.api.Assertions.*;

class LuhnAlgorithmTest {

    @Test
    /*
     * We add the throws to our method because we check so that it does not throw any error with valid input.
     */
    void expectedEqualsAndNoThrowGetCheckDigit() throws NullPointerException {
        assertDoesNotThrow(() -> LuhnAlgorithm.getCheckDigit("424242424242424"));
        assertEquals("2", LuhnAlgorithm.getCheckDigit("424242424242424"));

        assertEquals("3", LuhnAlgorithm.getCheckDigit("123459879"));
        assertEquals("8", LuhnAlgorithm.getCheckDigit("95121673"));
        assertEquals("7", LuhnAlgorithm.getCheckDigit("000010005"));
        assertEquals("0", LuhnAlgorithm.getCheckDigit("0000"));
    }
    @Test
    void expectedNotEqualsGetCheckDigit() throws NullPointerException{
        assertNotEquals("2", LuhnAlgorithm.getCheckDigit("42424242424242456"));

        assertNotEquals("3", LuhnAlgorithm.getCheckDigit("12345987967"));
        assertNotEquals("8", LuhnAlgorithm.getCheckDigit("9512167345"));
        assertNotEquals("7", LuhnAlgorithm.getCheckDigit("0000110005"));
        assertNotEquals("0", LuhnAlgorithm.getCheckDigit("0005760"));
    }
    @Test
    /*
     * We add the throws to our method because we check so that it does not throw any error with valid input.
     */
    void expectedTreAndNoThrowIsNumberValid() throws NullPointerException {
        assertDoesNotThrow(() -> LuhnAlgorithm.isNumbersValid("4242424242424242"));
        assertTrue(LuhnAlgorithm.isNumbersValid("4242424242424242"));
        assertTrue(LuhnAlgorithm.isNumbersValid("0000100057"));
        assertTrue(LuhnAlgorithm.isNumbersValid("1234598793"));
        assertTrue(LuhnAlgorithm.isNumbersValid("951216738"));
        assertTrue(LuhnAlgorithm.isNumbersValid("18"));
        assertTrue(LuhnAlgorithm.isNumbersValid("00"));
        assertTrue(LuhnAlgorithm.isNumbersValid("00000000"));
    }
    @Test
        /*
         * Valid input but false input.
         */
    void expectedFalseIsNumberValid() throws NullPointerException {
        assertFalse(LuhnAlgorithm.isNumbersValid("424242424242424284"));
        assertFalse(LuhnAlgorithm.isNumbersValid("000010005748"));
        assertFalse(LuhnAlgorithm.isNumbersValid("9512141236738"));
        assertFalse(LuhnAlgorithm.isNumbersValid("18012"));
        assertFalse(LuhnAlgorithm.isNumbersValid("01240"));
        assertFalse(LuhnAlgorithm.isNumbersValid("000000786900"));
    }
    /*
      * Valid input but false input.
     */
    @Test
    void expectedEqualsGetSum() throws InvocationTargetException, IllegalAccessException {
        assertEquals(44, getMethodFromName("getNumberSum").invoke(null, "811218987", true));
        assertEquals(50, getMethodFromName("getNumberSum").invoke(null, "8112189876", false));
    }
    /*
     * Valid input but false input.
     */
    @Test
    void expectedNotEqualsGetSum() throws InvocationTargetException, IllegalAccessException {
        assertNotEquals(42, getMethodFromName("getNumberSum").invoke(null, "811218987", true));
        assertNotEquals(51, getMethodFromName("getNumberSum").invoke(null, "8112189876", false));
    }
    @Test
    void isNumberValidExecutionBelow3Seconds() {
        assertTimeoutPreemptively(Duration.ofSeconds(3), () -> LuhnAlgorithm.isNumbersValid("894984984984984984984984984984984984984984984949849849849849849849849849819191981981981894984984984984984984984984984984984984984984949849849849849849849849849819191981981981986894984984984984984984984984984984984984984984949849849849849849849849849819191981981981986894984984984984984984984984984984984984984984949849849849849849849849849819191981981981986894984984984984984984984984984984984984984984949849849849849849849849849819191981981981986894984984984984984984984984984984984984984984949849849849849849849849849819191981981981986894984984984984984984984984984984984984984984949849849849849849849849849819191981981981986894984984984984984984984984984984984984984984949849849849849849849849849819191981981981986894984984984984984984984984984984984984984984949849849849212312389498498498498498498498498498498498498498498494984984984984984984984984981919198198198198689498498498498498498498498498498498498498498494984984984984984984984984981919198198198198689498498498498498498498498498498498498498498494984984984984984984984984981919198198198198689498498498498498498498498498498498498498498494984984984984984984984984981919198198198198689498498498498498498498498498498498498498498494984984984984984984984984981919198198198198689498498498498498498498498498498498498498498494984984984984984984984984981919198198198198689498498498498498498498498498498498498498498494984984984984984984984984981919198198198198689498498498498498498498498498498498498498498494984984984984984984984984981919198198198198689498498498498498498498498498498498498498498494984984984984984984984984981919198198198198689498498498498498498498498498498498498498498494984984984984984984984984981919198198198198689498498498498498498498498498498498498498498494984984984984984984984984981919198198198198689498498498498498498498498498498498498498498494984984984984984984984984981919198198198198689498498498498498498498498498498498498498498494984984984984984984984984981919198198198198689498498498498498498498498498498498498498498494984984984984984984984984981919198198198198689498498498498498498498498498498498498498498494984984984984984984984984981919198198198198689498498498498498498498498498498498498498498494984984984984984984984984981919198198198198689498498498498498498498498498498498498498498494984984984984984984984984981919198198198198689498498498498498498498498498498498498498498494984984984984984984984984981919198198198198689498498498498498498498498498498498498498498494984984984984984984984984981919198198198198689498498498498498498498498498498498498498498494984984984984984984984984981919198198198198689498498498498498498498498498498498498498498494984984984984984984984984981919198198198198689498498498498498498498498498498498498498498494984984984984984984984984981919198198198198684984984984984981919198198198198698"));
    }
    @Test
    void expectThrowOnBadInputIsNumberValid() {
        assertThrows(IllegalArgumentException.class, () -> LuhnAlgorithm.isNumbersValid("string"));
        assertThrows(IllegalArgumentException.class, () -> LuhnAlgorithm.isNumbersValid(""));
        assertThrows(IllegalArgumentException.class, () -> LuhnAlgorithm.isNumbersValid("12sd"));
        assertThrows(IllegalArgumentException.class, () -> LuhnAlgorithm.isNumbersValid(" 2 2 2 1"));
    }
    @Test
    void expectThrowOnBadInputGetCheckDigit() {
        assertThrows(IllegalArgumentException.class, () -> LuhnAlgorithm.getCheckDigit("string"));
        assertThrows(IllegalArgumentException.class, () -> LuhnAlgorithm.getCheckDigit(""));
        assertThrows(IllegalArgumentException.class, () -> LuhnAlgorithm.getCheckDigit("12sd"));
        assertThrows(IllegalArgumentException.class, () -> LuhnAlgorithm.getCheckDigit(" 2 2 2 1"));
    }

    /**
     * Will get a method by name from the LuhnAlgorithm class.
     * Will set accessibility to true so that even private methods can be executed.
     * @param name - name of the method
     * @return - method or null.
     */
    private Method getMethodFromName(String name) {
        Method[] methods = LuhnAlgorithm.class.getDeclaredMethods();
        for(Method m : methods) {
            if(m.getName().equals(name)) {
                m.setAccessible(true);
                return m;
            }
        }
        return null;
    }
}