package se.experis.com;

import se.experis.com.controller.Controller;
import se.experis.com.luhnalgo.LuhnAlgorithm;
import se.experis.com.view.View;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Program {

    public static void main(String[] args) {
        new Controller(new View()).start();
    }

}
