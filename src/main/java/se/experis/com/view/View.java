package se.experis.com.view;

import java.util.Scanner;

/**
 * Used to print information to the console.
 */
public class View {
    public String provideInputMessage() {
        System.out.println("Please provide input in the format [numbers] [checkDigit] (!exit!) to exit.");
        return new Scanner(System.in).nextLine();
    }

    /**
     * Prints information in a fancy way.
     * @param wholeInput - Input without splitting it.
     * @param inputCheckSum - The checksum entered by the user.
     * @param actualCheckSum - The checksum calculated.
     * @param digits - Amount of digits, checksum included.
     * @param valid - If the numbers checksum included is valid.
     * @param isCreditCard - If the numbers can be a creditcard or not.
     */
    public void printInformation(String wholeInput, String inputCheckSum, String actualCheckSum, int digits, boolean valid, boolean isCreditCard) {
        System.out.println("Input: " + wholeInput);
        System.out.println("CheckSums:");
        System.out.println("\tProvided: " + inputCheckSum);
        System.out.println("\tActual: " + actualCheckSum);
        System.out.println("\tResult: " + (valid?"\u001b[32mValid":"\u001b[31;1mInvalid") + "\u001b[0m");
        System.out.println("Digits: " + digits + " " + (isCreditCard?"(credit card)" : ""));

    }
    public void badInput() {
        System.out.println("The input can only be numbers.. Please try again");
    }
}
