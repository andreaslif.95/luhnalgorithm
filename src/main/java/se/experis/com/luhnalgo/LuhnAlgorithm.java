package se.experis.com.luhnalgo;

public class LuhnAlgorithm {
    /**
     * Will use getNumberSum to calculate the actual checkDigit which belongs to the numbers given.
     * Will only accept digits as input within the string. Therefore it uses the regex.
     * @param numbers - The string containing all numbers except the check Digit.
     * @return - Returns a string containing the check digit calculated from the numbers.
     */
    public static String getCheckDigit(String numbers) {
        if(!numbers.matches("[0-9]+")) {
            throw new IllegalArgumentException("Strings which holds numbers are only accepted as arguments");
        }
        int sum = getNumberSum(numbers, true);
        if(sum == 0) {
            return "0";
        }
        //We return the difference between the sum and the closest upper factor of ten.
        return ""+(10-(sum%10));
    }

    /**
     * Will use getNumberSum to calculate the actual numbers are valid or not.
     * Will only accept digits as input within the string. Therefore it uses the regex.
     * @param numbers - The string containing all numbers with the checkDigit.
     * @return - true if the number is valid or false if it's not.
     */
    public static boolean isNumbersValid(String numbers) {
        if(!numbers.matches("[0-9]+")) {
            throw new IllegalArgumentException("Strings which holds numbers are only accepted as arguments");
        }
        //Only numbers which hare dividable by 10 are considered valid.
        return getNumberSum(numbers, false)%10 == 0;
    }

    /**
     * Loops from the end of the string to the beginning multiplying each number by either 1 or 2 which alternates each iteration.
     * If the result of the multiplication is over 9 we add the individual numbers of the result together.
     * Each result is then added to a sum which is returned.
     * @param numbers - a string of numbers.
     * @param startWithTwo - If the calculation should multiple every other numbers by 2 start by the last number in the string or by the second.
     * @return - Sum of all the multiplications results.
     */
    private static int getNumberSum(String numbers, boolean startWithTwo) {
        int sum = 0;

        for(int i = numbers.length()-1; i >= 0; i--) {
            if(startWithTwo) {
                String s = (Integer.parseInt("" + numbers.charAt(i)) * 2) + "";
                int otherSum = 0;
                for (int j = 0; j < s.length(); j++) {
                    otherSum += Integer.parseInt(s.charAt(j) + "");
                }
                sum += otherSum;
            }
            else {
                sum += Integer.parseInt(numbers.charAt(i)+"");
            }
            startWithTwo = !startWithTwo;
        }

        return sum;
    }
}
