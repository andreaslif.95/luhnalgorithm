package se.experis.com.controller;

import se.experis.com.luhnalgo.LuhnAlgorithm;
import se.experis.com.view.View;

public class Controller {
    View view;
    public Controller(View view) {
       this.view = view;
    }

    /**
     * Used to handle the flow of the program.
     * Uses the view to print to the console.
     * Uses the LuhnAlgorithm to calculate.
     */
    public void start() {
        while(true) {
            String input = view.provideInputMessage();
            if (input.contains("!exit!")) {
                System.exit(0);
            }
            //Expecting input as [number] [digit] which seperates them by spaces. And should only contain two parts.
            String[] inputs = input.split(" ");
            if(inputs.length > 2) {
                view.badInput();
                continue;
            }
            try {
                String actualDigit = LuhnAlgorithm.getCheckDigit(inputs[0]);
                int wholeNumberLength = (inputs[0].length() + inputs[1].length()); //Concatinating the length of the both number inputs.
                boolean isNumberValid = LuhnAlgorithm.isNumbersValid(inputs[0]+inputs[1]);
                boolean isCreditCard = wholeNumberLength >= 16;
                view.printInformation(input, inputs[1], actualDigit, wholeNumberLength, isNumberValid, isCreditCard);
            }
            catch(Exception e) {
                view.badInput();
            }
        }

    }
}
